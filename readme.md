# Readme
Figured I should write one of these since i keep adding features and not explaining how to actually use them.

So, I'll update this whenever I add new features to explain how they work.

# x86 Specific

The last commit added the ability to choose to skip function calls if desired. Here is an example that does **NOT** use this:

```cpp
// Module32NextW hook
int m32nw_hook( Utility::registers * pRegs )
{
	if ( m32nw_og == 0 )
		m32nw_og = ( m32nw_type )Utility::pHookManager->GetCallableOriginal( 0, ( uintptr_t )m32nw_hook ); // This gets us a version of the function we can call without endlessly recursing

	auto ret_val = m32nw_og( ( HANDLE )pRegs->stack[ 1 ], ( LPMODULEENTRY32W )pRegs->stack[ 2 ] ); // Call that function so we can get its return value (if we care, we could skip this.)
	auto me = ( LPMODULEENTRY32W )pRegs->stack[ 2 ];
	auto name = std::wstring( me->szModule );
	std::transform( name.begin( ), name.end( ), name.begin( ), std::tolower );

	if ( is_in_whitelist( name ) )
		ret_val = m32nw_og( ( HANDLE )pRegs->stack[ 1 ], ( LPMODULEENTRY32W )pRegs->stack[ 2 ] ); // Skip this entry by replacing the data with the values of the NEXT module

	pRegs->eax = ret_val; // this sets the desired return value
	return 8;             // here, we return the stack fixup amount (how many arguments this function took).
    // In this instance, Module32NextW takes a handle and a LPMODULENETRY32W, so 4*2 == 8
}

// hook setup like so:
Utility::pHookManager->HookFunctionExt( ( uintptr_t )enumproc, ( uintptr_t )enumproc_hook, 5, false ); 
// NOTE: This is only possible if execute_first == false, as otherwise you couldve potentially fucked up the stack, etc etc.
```
In the example above, this allowed me to hook a function, execute the original, examine the returned values, and decide whether or not to let these through, or to replace them with something else, then skip the original function call.

Here is an example that executes the original so that we can get some return values, we can modify these if we choose. Then we can either skip, or execute the original function:

```cpp
int ntqueryvm_hook( Utility::registers * pRegs )
{
	if ( ntqvm_og == 0 )
		ntqvm_og = ( t_NtQueryVirtualMemory )Utility::pHookManager->GetCallableOriginal( 0, ( uintptr_t )ntqueryvm_hook );

	/*
		PVOID                    BaseAddress,
		MEMORY_INFORMATION_CLASS MemoryInformationClass,
		PVOID                    MemoryInformation,
		SIZE_T                   MemoryInformationLength,
		PSIZE_T                  ReturnLength
	*/
	HANDLE hproc = ( HANDLE )pRegs->stack[ 1 ];
	auto base_addr = pRegs->stack[ 2 ];
	auto mic = static_cast< MEMORY_INFORMATION_CLASS >( ( int )pRegs->stack[ 3 ] );
	auto buff = pRegs->stack[ 4 ];
	SIZE_T len = ( SIZE_T )pRegs->stack[ 5 ];
	SIZE_T * ret_len = ( SIZE_T * )pRegs->stack[ 6 ];

	if ( mic == MemoryBasicInformation )
	{
		auto ret_val = ntqvm_og( hproc, base_addr, mic, buff, len, ret_len );
		pRegs->eax = ret_val;
		if ( !ret_val )
		{
			// success.
            // do stuff to modify things
		}
	}
	else
	{
        // not something we want to modify, so........
		return Utility::HookManager::EXECUTE_TARGET_FUNCTION; // continue with actual function call
	}

	return 4 * 6; // skip actual function call 
}
```

 Of course, its just as easy to hook a function and decide to skip it by any other arbitrary means by simply returning with the correct fixup value, you don't necessarily have to call the original unless you need some values.